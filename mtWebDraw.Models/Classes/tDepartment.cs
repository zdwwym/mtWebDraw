﻿using System;

namespace mtWebDraw.Models
{
    /// <summary>
    /// 用户所属部门表
    /// </summary>
    public class tDepartment
    {
    
        /// <summary>
        /// 标识
        /// </summary>
        public virtual Guid ID { get; set; }
        
        /// <summary>
        /// 部门名称
        /// </summary>
        public virtual String FullName { get; set; }
        
        /// <summary>
        /// 介绍
        /// </summary>
        public virtual String Caption { get; set; }
        
    }
}